<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_email"]))
	{
		header("location: ./");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $user_id=$_SESSION["user_id"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where  eventname='$event_name'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_email"]);
            // unset($_SESSION["user_id"]);
            
            header("location: ./");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body id="bg">

<div class="container-fluid">
  <div class="">
      <div class="row">
            <div class="col-12 col-md-8 text-center">
                <div class="embed-responsive embed-responsive-16by9 margin_change">
     

                 <iframe src="video.php" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

   
              
        
                
                </div>
          </div>
            <div class="col-12 col-md-4">
                <div class="login">
                    <div id="polls" style="display:none;">
                      <div class="row mt-2">
                          <div class="col-12">
                              <iframe id="poll-question" src="#" width="100%" height="220" frameborder="0" scrolling="no"></iframe>
                          </div>
                      </div>    
                    </div>
                    <div class="login-form margin_change">
                        <form id="question-form" method="post" role="form">
                          <div class="row">
                            <div class="col-10">
                            <img src="img/logo.png" width="200px" alt="">
                                <h6 class="text-white">Ask your Question:</h6>
                                <div class="form-group">
                                   <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="4"></textarea>
                                </div>
                            </div>
                          </div>
                          <div class="row mt-3">
                            <div class="col-10">
                              <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_email']; ?>">
                              <input type="hidden"  name="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                                <input type="submit" id="webcastsubmit" class="btn  text-white" value="submit" alt="Submit">
                                <div id="message"></div>
                            </div>
                          </div>  
                    </form>
                    </div>
                </div>
                <a href="?action=logout" class="btn btn-sm text-white" id="logoutbutton">Logout</a>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
         }
});
}
setInterval(function(){ update(); }, 30000);

var pollTimer;
function chkPolls()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'chkpoll'},
         type: 'post',
         success: function(output) {
			   if(output != 0)
			   {    
                    if($('#poll-question').attr('src') != output)
                   {
                       $("#poll-question").attr("src", output);
                       $('#polls').css('display','');
                   }
			   }
               else{
                   $("#poll-question").attr("src", '#');
                   $('#polls').css('display','none');
               }
         }
    });
}
chkPolls();
pollTimer = setInterval(function(){ chkPolls(); }, 3000);
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-15"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-15');
</script>

</body>
</html>