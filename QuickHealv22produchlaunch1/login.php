<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Quick Heal</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body id="bg">

<div class="container-fluid">
  
    <div class="row ">

            <div class="col-12 col-md-6 text-center ">
                <img src="img/Asset.png" class="mobile margin_change" width="400px" alt="">
           </div>         
    
            <div class="col-12 col-md-6 col-lg-6 ">
                <div class="login">
                    <div class="login-form">
                        <div id="message"></div>
                        <form id="login-form" method="post" role="form">
                            <div class="row">
                                <div class="form-group col-12 col-md-6 offset-md-3">
                                    <img src="img/logo.png" class="mobile margin_change"  width="350px" alt="">
                                </div>  
                            </div>  
                            <div class="row">
                                    <div class="form-group col-12 col-md-6 offset-md-3 mt-4">
                                        <input class="form-control" type="email" id="user_email" name="user_email" placeholder="Email ID" required>
                                    </div>
                                    <div class="form-group col-12 col-md-6 offset-md-3 mt-4">
                                        <input class="form-control" type="number" id="user_email" name="mobile" placeholder="Enter Mobile Number" required>
                                    </div>
                            </div>

                            <div class="row mt-3">
                                <div class="form-group col-12 col-md-6 offset-md-3 mt-4">
                                    <input type="submit" id="submit" class="btn btn-submit" value="LOGIN" alt="Submit">
                                </div>
                            </div>  
                         </form>
                    </div>
                </div>
            </div>
           
      </div>
  </div>
    
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#login-form', function()
    {  
          $.post('chkforlogin.php', $(this).serialize(), function(data)
          {
             // console.log(data);
              if(data == 's')
              {
                window.location.href='webcast.php';  
              }
              else if (data == '-1')
              {
                  alert('You are already logged in. Please logout and try again.');
                  return false;
              }
              else
              {
                  alert(data);
                  return false;
              }
          });
      
      return false;
    });
});
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-15"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-15');
</script>

</body>
</html>